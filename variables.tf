variable "environment" {
  type = string
  description = "The environment development/staging/test/production"
  default = "staging"
}

variable "minimum_protocol_version" {
  type = string
  description = "The minimum protocol version per this table: https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/secure-connections-supported-viewer-protocols-ciphers.html"
  default = "TLSv1.2_2019"
}

variable "name" {
  type = string
  description = "The name of the deployment"
}

variable "domain_names" {
  type = list(string)
  description = "The domain names that will be used to access this distribution"
}

# variable "public_dns_zone_name" {
#   type = string
#   description = "The name of the public Route53 zone where this module will automatically create DNS entries at name-cdn.<zone>"
#   default = "internaldns-snaptravel.com."
# }

# variable "private_dns_zone_name" {
#   type = string
#   description = "The name of the private Route53 zone where this module will automatically create DNS entries at name-cdn.<zone>"
#   default = "internaldns-snaptravel.com."
# }

variable "certificate_arn" {
  type = string
  description = "The ACM SSL certificate ARN"
}
