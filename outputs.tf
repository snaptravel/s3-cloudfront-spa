output "cloudfront_distribution" {
  value = aws_cloudfront_distribution.s3_distribution
}

output "s3_bucket" {
  value = aws_s3_bucket.spa_bucket
}

output "s3_bucket_name" {
  value = aws_s3_bucket.spa_bucket.bucket
}

# output "logs_bucket" {
#   value = aws_cloudfront_distribution.s3_distribution
# }
