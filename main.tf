data "aws_caller_identity" "current" {}

resource "aws_cloudfront_origin_access_identity" "cloudfront_origin_access_identity" {
  comment = "Cloudfront Origin Access Identity for ${var.name}"
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = [ "s3:Get*", "s3:List*" ]
    resources = [
      "${aws_s3_bucket.spa_bucket.arn}",
      "${aws_s3_bucket.spa_bucket.arn}/*"
    ]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.cloudfront_origin_access_identity.iam_arn]
    }
  }
}

# data "aws_iam_policy_document" "cdn_logs_policy" {
#   statement {
#     actions   = ["s3:GetObject"]
#     resources = ["${aws_s3_bucket.spa_bucket.arn}/*"]

#     principals {
#       type        = "AWS"
#       identifiers = [aws_cloudfront_origin_access_identity.cloudfront_origin_access_identity.iam_arn]
#     }
#   }
# }

resource "aws_s3_bucket" "spa_bucket" {
  bucket = "${var.name}-bucket"
  acl    = "private"
	force_destroy = "true"
}

# resource "aws_s3_bucket" "cdn_logs_bucket" {
#   bucket = "${var.name}-cdn-logs-bucket"
#   acl    = "private"
# 	force_destroy = "true"
# }

resource "aws_s3_bucket_policy" "bucket_policy" {
  bucket = aws_s3_bucket.spa_bucket.bucket
  policy = data.aws_iam_policy_document.s3_policy.json
}

resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.spa_bucket.bucket_regional_domain_name
    origin_id = "${var.name}-default-origin"
    s3_origin_config {
      origin_access_identity = aws_cloudfront_origin_access_identity.cloudfront_origin_access_identity.cloudfront_access_identity_path
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Cloudfront distribution for ${var.name}"
  default_root_object = "index.html"

  default_cache_behavior {
    target_origin_id = "${var.name}-default-origin"
    allowed_methods = [ "GET", "HEAD", "OPTIONS" ]
    cached_methods  = [ "GET", "HEAD", "OPTIONS" ]
    compress        = true
    viewer_protocol_policy = "redirect-to-https"

    # Setting the default values here will enable "Use Origin Cache Headers" in the AWS UI
    min_ttl                = 0
    default_ttl            = 86400
    max_ttl                = 31536000

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  custom_error_response {
    error_caching_min_ttl = 60
    error_code = 404
    response_code = 200
    response_page_path = "/index.html"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  # logging_config {
  #   include_cookies = false
  #   bucket          = aws_s3_bucket.cdn_logs_bucket.bucket_domain
  # }

  aliases = var.domain_names

  tags = {
    Environment = var.environment
    Name        = var.name
  }

  viewer_certificate {
    acm_certificate_arn = var.certificate_arn
    ssl_support_method = "sni-only"
    minimum_protocol_version = var.minimum_protocol_version
  }
}
